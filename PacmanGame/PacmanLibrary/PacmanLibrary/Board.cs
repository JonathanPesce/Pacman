﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanLibrary
{
    public class Board
    {
        private Tile[,] Grid;

        public Board(Tile[,] Grid, int startX, int startY)
        {
            this.Grid = Grid;
            this.startX = startX;
            this.startY = startY;
        }

        public int startX
        {
            get;
            private set;
        }

        public int startY
        {
            get;
            private set;
        }
        public Tile this[int x, int y]
        {
            get { return this.Grid[x, y]; }
        }

        public int Height
        {
            get { return this.Grid.GetLength(1); }
        }

        public int Width
        {
            get { return this.Grid.GetLength(0); }
        }

        /// <summary>
        /// Checks to see if a specific tile is accessible
        /// </summary>
        /// <param name="posX">X position of the tile</param>
        /// <param name="posY">Y position of the tile</param>
        /// <returns></returns>
        public bool IsAccessible(int posX, int posY)
        {
                return Grid[posX, posY].Accessible;
        }

        public bool IsWarper(int posX, int posY)
        {
            return false; //Code after
        }
    }
}
