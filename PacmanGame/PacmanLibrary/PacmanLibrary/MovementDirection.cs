﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanLibrary
{
    public enum MovementDirection
    {
        Up, Down, Left, Right
    }
}
