﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanLibrary
{
    public class Pinky:Ghost
    {
        private Pacman pacman;
        private int scatterX;
        private int scatterY;

        public Pinky(int startX, int startY, Pacman pacman, Board board) : base(startX, startY, board)
        {
            this.pacman = pacman;
            this.scatterX = 2;
            this.scatterY = -2;
        }

        public override void move()
        {
            if (pacman.direction == MovementDirection.Down)
            {
                base.move(pacman.PosX, pacman.PosY + 4);
            } else if (pacman.direction == MovementDirection.Up)
            {
                base.move(pacman.PosX, pacman.PosY - 4);
            }else if (pacman.direction == MovementDirection.Left)
            {
                base.move(pacman.PosX - 4, pacman.PosY);
            }else
            {
                base.move(pacman.PosX + 4, pacman.PosY);
            }
        }

        public override void scatter()
        {
            base.scatter(scatterX, scatterY);
        }

        
    }
}
