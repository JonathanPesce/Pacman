﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanLibrary
{
    public class Inky : Ghost
    {
        private int scatterX;
        private int scatterY;
        private Pacman pacman;
        private Blinky blinky;

        public Inky(int startX, int startY, Pacman pacman, Board board, Blinky blinky):base(startX, startY, board)
        {
            this.scatterX = 27;
            this.scatterY = 29;
            this.pacman = pacman;
            this.blinky = blinky;
        }

        public override void move()
        { 
            if(pacman.direction == MovementDirection.Down)
            {
                distanceBetweenPointAndBlinkyDoubled(pacman.PosX, pacman.PosY + 1);
            } else if (pacman.direction == MovementDirection.Up)
            {
                distanceBetweenPointAndBlinkyDoubled(pacman.PosX, pacman.PosY - 1);
            } else if (pacman.direction == MovementDirection.Left)
            {
                distanceBetweenPointAndBlinkyDoubled(pacman.PosX - 1, pacman.PosY);
            } else
            {
                distanceBetweenPointAndBlinkyDoubled(pacman.PosX + 1, pacman.PosY);
            }
        }

        public override void scatter()
        {
            base.scatter(scatterX, scatterY);
        }

        private void distanceBetweenPointAndBlinkyDoubled(int posX, int posY)
        {
            int posXOfDestination = (pacman.PosX - blinky.PosX) + pacman.PosX;
            int posYOfDestination = (pacman.PosY - blinky.PosY) + pacman.PosY;

            base.move(posXOfDestination, posYOfDestination);
        }
    }
}
