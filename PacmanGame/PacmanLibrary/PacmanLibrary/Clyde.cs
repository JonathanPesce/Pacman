﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanLibrary
{
    public class Clyde:Ghost
    {
        private Pacman pacman;
        private int scatterX;
        private int scatterY;

        public Clyde(int startX, int startY, Pacman pacman, Board board) : base(startX, startY, board)
        {
            this.scatterX = 0;
            this.scatterY = 29;
            this.pacman = pacman;
        }

        public override void move()
        {
            if (distanceBetweenClydeAndPacman() > 8)
            {
                base.move(pacman.PosX, pacman.PosY);
            } else
            {
                base.move(scatterX, scatterY);
            }
        }

        public override void scatter()
        {
            base.scatter(scatterX, scatterY);
        }

        public double distanceBetweenClydeAndPacman()
        {
            return Math.Sqrt(Math.Pow(pacman.PosX - this.PosX, 2) + Math.Pow(pacman.PosY - this.PosY, 2));
        }
    }
}
