﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanLibrary
{
    public delegate void pointEaten(int points, bool isEnergizer);
    public class Path : Tile
    {
        private bool pointIsAvailable;
        public event pointEaten pointHasBeenEaten;
        private int points;

        public Path(Direction dir, bool pointOn, pointEaten scoreUpdate, bool energizer) : base(dir)
        {
            pointIsAvailable = pointOn;
            points = 10;
            pointHasBeenEaten += scoreUpdate;
            this.pointIsEnergizer = energizer;
        }

        public override bool Accessible
        {
            get { return true; }
        }

        public bool pointAvailable
        {
            get { return pointIsAvailable; }
            set
            {
                if (pointIsAvailable)
                {
                    pointHasBeenEaten?.Invoke(points, pointIsEnergizer);
                    pointIsAvailable = value;
                }
            }
        }

        public bool pointIsEnergizer
        {
            get;
            private set;
        }
    }
}
