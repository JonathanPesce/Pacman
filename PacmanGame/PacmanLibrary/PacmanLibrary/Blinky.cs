﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanLibrary
{
    public class Blinky : Ghost
    {
        private Pacman pacman;
        int scatterX;
        int scatterY;

        public Blinky(int startX, int startY, Pacman pacman, Board board) : base(startX, startY, board)
        {
            this.scatterX = 26;
            this.scatterY = -2;
            this.pacman = pacman;
        }

        public override void move()
        {
            base.move(pacman.PosX, pacman.PosY);
        }

        public override void scatter()
        {
            base.scatter(scatterX, scatterY);
        }

    }
}
