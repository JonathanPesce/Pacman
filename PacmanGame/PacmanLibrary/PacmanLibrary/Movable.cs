﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanLibrary
{
    public abstract class Movable
    {
        public Movable(int startPosX, int startPosY)
        {
            PosX = startPosX;
            PosY = startPosY;
        }

        public int PosX
        {
            get;
            set;
        }

        public int PosY
        {
            get;
            set;
        }

        public virtual void move(MovementDirection direction, Board board)
        {
            bool isTeleport = false;
            if (board[PosX, PosY] is Teleport)
            {
                isTeleport = true;
            }

            if (direction == MovementDirection.Down)
            {
                if (isTeleport && PosY == board.Height - 1)
                {
                    Teleport teleport = (Teleport)board[PosX, PosY];
                    PosX = teleport.Destination.PosX;
                    PosY = teleport.Destination.PosY;
                }
                else if (board.IsAccessible(PosX, PosY + 1))
                {
                    PosY++;
                }
            }
            else if (direction == MovementDirection.Up)
            {
                if (isTeleport && PosY == 0)
                {
                    Teleport teleport = (Teleport)board[PosX, PosY];
                    PosX = teleport.Destination.PosX;
                    PosY = teleport.Destination.PosY;
                }
                else if (board.IsAccessible(PosX, PosY - 1))
                {
                    PosY--;
                }
            }
            else if (direction == MovementDirection.Left)
            {
                if (isTeleport && PosX == 0)
                {
                    Teleport teleport = (Teleport)board[PosX, PosY];
                    PosX = teleport.Destination.PosX;
                    PosY = teleport.Destination.PosY;
                }
                else if (board.IsAccessible(PosX - 1, PosY))
                {
                    PosX--;
                }
            }
            else if (direction == MovementDirection.Right)
            {
                if (isTeleport && PosX == board.Width - 1)
                {
                    Teleport teleport = (Teleport)board[PosX, PosY];
                    PosX = teleport.Destination.PosX;
                    PosY = teleport.Destination.PosY;
                }
                else if (board.IsAccessible(PosX + 1, PosY))
                {
                    PosX++;
                }
            }
        }
    }
}
