﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanLibrary
{
    public class Wall : Tile
    {
        public Wall(Direction dir) : base(dir)
        {

        }

        //Don't override Accessible as a character is not allowed on a wall tile
    }
}
