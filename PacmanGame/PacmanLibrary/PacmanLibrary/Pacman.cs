﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanLibrary
{
    public delegate void Move(MovementDirection dir);
    public class Pacman : Movable
    {
        public event Move pacmanMoved;
        public MovementDirection direction;

        public Pacman(int startX, int startY) : base(startX, startY)
        {

        }

        public override void move(MovementDirection direction, Board board)
        {
            this.direction = direction;
            base.move(direction, board);
            this.pacmanMoved?.Invoke(direction);
        }


    }
}
