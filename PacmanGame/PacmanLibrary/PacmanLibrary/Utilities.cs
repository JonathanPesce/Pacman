﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanLibrary
{
    public static class Utilities
    {
        public static Board ParseBoard(String location, GameControl gameControl)
        {
            String fileContent = File.ReadAllText(location + ".txt");
            String[] lines = fileContent.Split('\n');
            lines = lines.Where(x => !(x.Equals(""))).ToArray();

            String[] dimensions = lines[0].Split(' ');
            int width = Int32.Parse(dimensions[0]);
            int height = Int32.Parse(dimensions[1]);
            
            int startX = Int32.Parse(dimensions[2]);
            int startY = Int32.Parse(dimensions[3]);

            Teleport teleport = null;
            int teleportCounter = 0;

            Tile[,] board = new Tile[width, height];

            for (int i = 1; i < lines.Length; i++)
            {
                String[] line = lines[i].Split(' ');

                for (int j = 0; j < line.Length; j++)
                {
                    Tile tile = null;
                    String letter = line[j];
                    if (letter.Length > 1)
                    {
                        letter = letter.First().ToString();
                    }

                    if (letter.Equals("P"))
                    {
                        tile = new Path(Direction.None, true, gameControl.increaseScore, false);
                    }
                    else if (letter.Equals("W"))
                    {
                        tile = new Wall(Direction.None);
                    }
                    else if (letter.Equals("B"))
                    {
                        tile = new BlackSpace();
                    } else if (letter.Equals("N"))
                    {
                        tile = new Path(Direction.None, true, gameControl.increaseScore, true);
                    }
                    else if (letter.Equals("E"))
                    {
                        tile = new Path(Direction.None, false, gameControl.increaseScore, false);
                    }
                    else if (letter.Equals("T"))
                    {
                        tile = new Teleport(j, i - 1);
                        if (teleportCounter == 0)
                        {
                            teleport = (Teleport)tile;
                            teleportCounter++;
                        }
                        else
                        {
                            teleport.Destination = (Teleport)tile;
                            ((Teleport)tile).Destination = teleport;
                        }
                    }

                    board[j, i - 1] = tile;
                }
            }
            return new Board(board, startX, startY);
        }
    }
}
