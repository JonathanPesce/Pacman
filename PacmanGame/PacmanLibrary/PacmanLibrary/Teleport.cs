﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanLibrary
{
    public class Teleport : Tile
    {
        public Teleport(int posX, int posY) : base(Direction.None)
        {
            PosX = posX;
            PosY = posY;
        }

        public override bool Accessible
        {
            get { return true; }
        }

        public int PosX
        {
            get;
            private set;
        }

        public int PosY
        {
            get;
            private set;
        }

        public Teleport Destination
        {
            get;
            set;
        }

    }
}
