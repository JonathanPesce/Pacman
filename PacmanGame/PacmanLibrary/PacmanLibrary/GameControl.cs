﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanLibrary
{
    public class GameControl
    {
        private Board board;
        private int level;
        private int score;
        private int numberOfDotsEaten;
        private bool energized;

        public GameControl()
        {
            board = Utilities.ParseBoard("./LEVEL1", this);
            level = 2;
            Pacman = new Pacman(board.startX, board.startY);
            Pacman.pacmanMoved += this.pacmanMoved;
            Blinky = new Blinky(14, 11, Pacman, board);
            Pinky = new Pinky(14, 13, Pacman, board);
            Inky = new Inky(12, 13, Pacman, board, Blinky);
            Clyde = new Clyde(16, 13, Pacman, board);
            numberOfDotsEaten = 0;
            Lives = 3;
            energized = false;
        }

        public void reset()
        {
            Lives--;
            resestCharactersPosition(Blinky, 14, 11);
            resestCharactersPosition(Pinky, 14, 12);
            resestCharactersPosition(Inky, 12, 13);
            resestCharactersPosition(Clyde, 16, 13);
            resestCharactersPosition(Pacman, 14, 23);
            Pinky.inHouse = true;
            Inky.inHouse = true;
            Clyde.inHouse = true;
            numberOfDotsEaten = 0;
            Energized = false;
        }

        private void resestCharactersPosition(Movable character, int posX, int posY)
        {
            character.PosX = posX;
            character.PosY = posY;
        }

        public void frightenedGhost()
        {
            Blinky.frightened();
            Pinky.frightened();

            if (numberOfDotsEaten >= 30)
                Inky.frightened();
            if (numberOfDotsEaten >= 82)
                Clyde.frightened();
        }
        public void moveGhost()
        {
            Blinky.move();
            Pinky.move();

            if(numberOfDotsEaten >= 30)
                Inky.move();
            if(numberOfDotsEaten >= 82)
                Clyde.move();
        }

        public void scatterGhost()
        {
            Blinky.scatter();
            Pinky.scatter();

            if (numberOfDotsEaten >= 30)
                Inky.scatter();
            if (numberOfDotsEaten >= 82)
                Clyde.scatter();
        }

        public  void pacmanMoved(MovementDirection dir)
        {
            if (board[Pacman.PosX, Pacman.PosY] is Path)
                ((Path)board[Pacman.PosX, Pacman.PosY]).pointAvailable = false;
        }

        public bool checkGameOver()
        {   
            if (!Blinky.isFrightened && Pacman.PosX == Blinky.PosX && Pacman.PosY == Blinky.PosY)
                return true;
            if (!Pinky.isFrightened && Pacman.PosX == Pinky.PosX && Pacman.PosY == Pinky.PosY)
                return true;
            if (!Inky.isFrightened && Pacman.PosX == Inky.PosX && Pacman.PosY == Inky.PosY)
                return true;
            if (!Clyde.isFrightened && Pacman.PosX == Clyde.PosX && Pacman.PosY == Clyde.PosY)
                return true;

            return false;
        }

        public void checkIfGhostWasEaten()
        {
            if (Pacman.PosX == Blinky.PosX && Pacman.PosY == Blinky.PosY && Blinky.isFrightened)
            {
                Blinky.eaten = true;
                score += 200;
            }

            if (Pacman.PosX == Pinky.PosX && Pacman.PosY == Pinky.PosY && Pinky.isFrightened)
            {
                Pinky.eaten = true;
                score += 200;
            }

            if (Pacman.PosX == Inky.PosX && Pacman.PosY == Inky.PosY && Inky.isFrightened)
            {
                Inky.eaten = true;
                score += 200;
            }

            if (Pacman.PosX == Clyde.PosX && Pacman.PosY == Clyde.PosY && Clyde.isFrightened)
            {
                Clyde.eaten = true;
                score += 200;
            }
        }

        public void increaseScore(int points, bool isEnergizer)
        {
            score += points;
            numberOfDotsEaten++;

            if (isEnergizer)
            {
                Energized = true;
            }
        }

        public Tile this[int x, int y]
        {
            get { return board[x, y]; }
        }

        public Board Board
        {
            get { return board; }
        }

        public Pacman Pacman
        {
            get;
            private set;
        }

        public Blinky Blinky
        {
            get;
            private set;
        }

        public Pinky Pinky
        {
            get;
            private set;
        }

        public Inky Inky
        {
            get;
            private set;
        }

        public Clyde Clyde
        {
            get;
            private set;
        }

        public int Score
        {
            get { return score; }
        }

        public int Lives
        {
            get;
            set;
        }

        public bool Energized
        {
            get
            {
                return energized;
            }

            set
            {
                energized = value;
                Blinky.isFrightened = value;
                Pinky.isFrightened = value;
                Inky.isFrightened = value;
                Clyde.isFrightened = value;
            }
        }
    }
}
