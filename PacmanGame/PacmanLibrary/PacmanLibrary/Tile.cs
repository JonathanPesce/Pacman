﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanLibrary
{
    public abstract class Tile
    {
        public Tile(Direction dir)
        {
            this.Direction = dir;
        }

        /// <summary>
        /// This determines the direction of a tile
        /// </summary>
        public Direction Direction
        {
            get;
            private set;
        }


        /// <summary>
        /// This property will allow characters to know if they are able to 
        /// step on this tile.
        /// </summary>
        public virtual bool Accessible
        {
            get { return false; }
            set { /*Will do nothing*/}
        }
    }
}
