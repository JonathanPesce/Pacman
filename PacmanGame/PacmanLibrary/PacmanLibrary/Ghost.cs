﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanLibrary
{
    public abstract class Ghost : Movable
    {
        private Board board;
        private MovementDirection direction;
        private bool switchedFromScatter;
        public bool inHouse;
        public bool eaten;
        
        
        public Ghost(int startX, int startY, Board board) : base(startX, startY)
        {
            this.board = board;
            direction = MovementDirection.Up;
            switchedFromScatter = false;
            if (startX == 14 && startY == 11)
            {
                inHouse = false;
            } else
            {
                inHouse = true;
            }
        }

        public void frightened() 
        {
            if (inHouse)
            {
                leaveHouse();
            } else if (eaten)
            {
                moveToHouse();
            }
            else
            {
                if (!currentDirectionIsOnlyOption())
                {
                    this.direction = randomDirection();
                }
                base.move(this.direction, this.board);
            }
        }

        private MovementDirection randomDirection()
        {
            List<MovementDirection> choices = new List<MovementDirection>();
           if(board.IsAccessible(PosX + 1, PosY) && this.direction != MovementDirection.Left)
                choices.Add(MovementDirection.Right);
            
            if (board.IsAccessible(PosX - 1, PosY) && this.direction != MovementDirection.Right)
                choices.Add(MovementDirection.Left);

            if (board.IsAccessible(PosX, PosY + 1) && this.direction != MovementDirection.Up)
                choices.Add(MovementDirection.Down);

            if (board.IsAccessible(PosX, PosY - 1) && this.direction != MovementDirection.Down)
                choices.Add(MovementDirection.Up);

            Random random = new Random();
            int temp = (int)(random.Next() % choices.Count);
            return choices[temp];
        }

        public void scatter(int posXOfScatterTile, int posYOfScatterTile)
        {
            if (inHouse)
            {
                leaveHouse();
            }
            else if (eaten)
            {
                moveToHouse();
            }
            else if (switchedFromScatter)
            {
                base.move(oppositeDirection(), board);
                switchedFromScatter = false;
            }
            else if (currentDirectionIsOnlyOption())
            {
                base.move(direction, board);
            }
            else
            {
                base.move(directionToMove(posXOfScatterTile, posYOfScatterTile), board);
            }
        }

        /// <summary>
        /// This method will move Blinky based on what is the most effective way of getting to pacman.
        /// This method will take into consideration the original version of blinky's path chossing, 
        /// meaning it may not always choose what is the best route in order to catch pacman.
        /// </summary>
        public void move(int posXOfTile, int posYOfTile)
        {
            if (inHouse)
            {
                    leaveHouse();
            }
            else if (eaten)
            {
                moveToHouse();
            }
            else if (currentDirectionIsOnlyOption())
            {
                base.move(direction, board);
            }
            else
            {
                base.move(directionToMove(posXOfTile, posYOfTile), board);
            }
            switchedFromScatter = true;
        }

        public void moveWithoutChecking(int posXOfTile, int posYOfTile)
        {
            if (currentDirectionIsOnlyOption())
            {
                base.move(direction, board);
            }
            else
            {
                base.move(directionToMove(posXOfTile, posYOfTile), board);
            }
        }

        private MovementDirection directionToMove(int posXOfTile, int posYOfTile)
        {
            double up = testDistanceBetweenTwoPoints(this.PosX, this.PosY - 1, posXOfTile, posYOfTile, MovementDirection.Down);
            double down = testDistanceBetweenTwoPoints(this.PosX, this.PosY + 1, posXOfTile, posYOfTile, MovementDirection.Up);
            double left = testDistanceBetweenTwoPoints(this.PosX - 1, this.PosY, posXOfTile, posYOfTile, MovementDirection.Right);
            double right = testDistanceBetweenTwoPoints(this.PosX + 1, this.PosY, posXOfTile, posYOfTile, MovementDirection.Left);

            if (up < down && up < left && up < right)
            {
                direction = MovementDirection.Up;
                return MovementDirection.Up;

            }
            else if (down < left && down < right)
            {
                direction = MovementDirection.Down;
                return MovementDirection.Down;
            }
            else if (left < right)
            {
                direction = MovementDirection.Left;
                return MovementDirection.Left;
            }

            direction = MovementDirection.Right;
            return MovementDirection.Right;
        }





        /// <summary>
        /// This method will return the distance between a tile and pacman. If the tile that is given
        /// is not accesible, the default value of 1000 will be returned.
        /// </summary>
        /// <param name="posX"></param>
        /// <param name="posY"></param>
        /// <returns></returns>
        private double testDistanceBetweenTwoPoints(int posX, int posY, int posX2, int posY2, MovementDirection dir)
        {
            if (!board[posX, posY].Accessible || dir == direction)
                return 1000;

            return Math.Sqrt(Math.Pow(posX - posX2, 2) + Math.Pow(posY - posY2, 2));
        }

        /// <summary>
        /// This method will verify to see if the Blinky's current direction is the only
        /// direction that he may continue on, keeping in mind the fact that a ghost can
        /// never backtrack his steps while on chase mode.
        /// </summary>
        /// <returns></returns>
        private bool currentDirectionIsOnlyOption()
        {
            if (direction == MovementDirection.Down || direction == MovementDirection.Up)
            {
                if (board.IsAccessible(this.PosX - 1, this.PosY) || board.IsAccessible(this.PosX + 1, this.PosY))
                    return false;
            }
            else
            {
                if (board.IsAccessible(this.PosX, this.PosY - 1) || board.IsAccessible(this.PosX, this.PosY + 1))
                    return false;
            }

            return true;
        }

        private MovementDirection oppositeDirection()
        {
            if (this.direction == MovementDirection.Down)
            {
                return MovementDirection.Up;
            } else if (this.direction == MovementDirection.Up)
            {
                return MovementDirection.Down;
            } else if (this.direction == MovementDirection.Left)
            {
                return MovementDirection.Right;
            }

            return MovementDirection.Left;
        }

        public void leaveHouse()
        {
            if (PosX == 14 && PosY == 13)
            {
                PosY = 12;
            } else if (PosX == 14 && PosY == 12)
            {
                PosY = 11;
                inHouse = false;
            } else 
            {
                this.moveWithoutChecking(14, 13);
            }
        }

        private void moveToHouse()
        {
            if (PosX == 14 && PosY == 11)
            {
                PosY = 12;
            }
            else
            {
                base.move(directionToMove(14, 12), board);
                if (PosX == 14 && PosY == 13)
                {
                    eaten = false;
                    inHouse = true;
                    this.isFrightened = false;
                }
            } 
        }

        public bool isFrightened
        {
            get;
            set;
        }

        public abstract void move();
        public abstract void scatter();
        
    }
}
