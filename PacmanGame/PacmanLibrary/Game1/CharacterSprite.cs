﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using PacmanLibrary;

namespace Pacman
{
    public class CharacterSprite : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private PacmanGame game;
        private GameControl gameControl;
        private int counter = 0;
        private bool open = false;

        //Add the textures for the ghost
        private Texture2D pacman;
        private Texture2D pacmanLeft;
        private Texture2D pacmanRight;
        private Texture2D pacmanUp;
        private Texture2D pacmanDown;
        private Texture2D blinky;
        private Texture2D pinky;
        private Texture2D inky;
        private Texture2D clyde;
        private Texture2D frightened;
        private Texture2D eaten;

        public CharacterSprite(PacmanGame game, GameControl gameControl) : base(game)
        {
            this.game = game;
            this.gameControl = gameControl;
        }

        /// <summary>
        /// I don't know what this does for now.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            this.spriteBatch = new SpriteBatch(GraphicsDevice);

            this.pacman = game.Content.Load<Texture2D>("Pacman");
            this.pacmanLeft = game.Content.Load<Texture2D>("Left");
            this.pacmanRight = game.Content.Load<Texture2D>("Right");
            this.pacmanUp = game.Content.Load<Texture2D>("Top");
            this.pacmanDown = game.Content.Load<Texture2D>("Bottom");
            this.blinky = game.Content.Load<Texture2D>("Blinky");
            this.pinky = game.Content.Load<Texture2D>("Pinky");
            this.inky = game.Content.Load<Texture2D>("Inky");
            this.clyde = game.Content.Load<Texture2D>("Clyde");
            this.frightened = game.Content.Load<Texture2D>("Frightened");
            this.eaten = game.Content.Load<Texture2D>("EatenGhost");

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            this.spriteBatch.Begin();

            if (open)
            {
                this.spriteBatch.Draw(pacman, new Rectangle(gameControl.Pacman.PosX * 30, gameControl.Pacman.PosY * 30 + 35, 30, 30), Color.White);
            } else
            {
                this.spriteBatch.Draw(pacmanDirectionSprite(), new Rectangle(gameControl.Pacman.PosX * 30, gameControl.Pacman.PosY * 30 + 35, 30, 30), Color.White);
            }

            counter++;
            if (counter == 10)
            {
                counter = 0;
                if (open)
                {
                    open = false;
                } else
                {
                    open = true;
                }
            }


            this.DrawGhost(gameControl.Blinky, spriteBatch, blinky);
            this.DrawGhost(gameControl.Pinky, spriteBatch, pinky);
            this.DrawGhost(gameControl.Inky, spriteBatch, inky);
            this.DrawGhost(gameControl.Clyde, spriteBatch, clyde);

            this.spriteBatch.End();
            base.Draw(gameTime);
        }

        private void DrawGhost(Ghost ghost, SpriteBatch spriteBatch, Texture2D pic)
        {
            if (ghost.eaten)
            {
                pic = eaten;
            } else if (ghost.isFrightened)
            {
                pic = frightened;
            }

            spriteBatch.Draw(pic, new Rectangle(ghost.PosX * 30, ghost.PosY * 30 + 35, 30, 30), Color.White);
        }

        private Texture2D pacmanDirectionSprite()
        {
            if (gameControl.Pacman.direction == MovementDirection.Down)
                return this.pacmanDown;
            if (gameControl.Pacman.direction == MovementDirection.Left)
                return this.pacmanLeft;
            if (gameControl.Pacman.direction == MovementDirection.Up)
                return this.pacmanUp;
            return this.pacmanRight;
        }
    }
}