﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using PacmanLibrary;

namespace Pacman
{
    public class ScoreBoard : DrawableGameComponent
    {
        private GameControl gameControl;
        private SpriteBatch spriteBatch;
        private SpriteFont font;
        private PacmanGame game;
        private Texture2D pacmanLives;

        public ScoreBoard(PacmanGame game, GameControl gameControl) : base(game)
        {
            this.game = game;
            this.gameControl = gameControl;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            this.spriteBatch = new SpriteBatch(GraphicsDevice);
            this.font = game.Content.Load<SpriteFont>("font");
            this.pacmanLives = game.Content.Load<Texture2D>("Right");

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            this.spriteBatch.Begin();
            spriteBatch.DrawString(font, "HIGH SCORE: " + gameControl.Score, new Vector2(game.graphics.PreferredBackBufferWidth / 2 - 75, 10), Color.White);
            for(int i = 0; i < gameControl.Lives; i++)
            {
                spriteBatch.Draw(pacmanLives, new Rectangle(20 + i * 30, game.graphics.PreferredBackBufferHeight - 30, 30, 30), Color.White);
            }
            this.spriteBatch.End();

            base.Draw(gameTime);
        }

    }
}
