﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PacmanLibrary;
using System.Timers;

namespace Pacman
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class PacmanGame : Game
    {
        public GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private BoardSprite boardSprite;
        private CharacterSprite charSprite;
        private ScoreBoard scoreBoard;
        private GameControl gameControl = new GameControl();
        private int counter = 0;
        private int threshold = 8;
        private int ghostCounter = 0;
        private int ghostThreshold = 10;
        private bool gameOver = true;
        private bool scatter = true;
        private Timer energizerTimers;
        private Timer timer;
        private int[] ghostPattern = {5000, 20000, 7000, 20000, 5000, 20000, 5000};
        private int ghostPhases = 0;
        private bool energized;

        private void gameTimer()
        {
            if (ghostPhases == ghostPattern.Length)
            {
                scatter = false;
            } else
            {
                timer = new Timer(); 
                timer.Interval = ghostPattern[ghostPhases];
                timer.Elapsed += timerHandler;
                timer.Start();
                ghostPhases++;
            }
        }

        private void timerHandler(object sender, ElapsedEventArgs e)
        {
            if (scatter)
            {
                scatter = false;
            } else
            {
                scatter = true;
            }
            timer.Dispose();
            gameTimer();
        }

        private void energizedOver(object sender, ElapsedEventArgs e)
        {
            energizerTimers.Stop();
            timer.Start();
            energized = false;
            gameControl.Energized = false;
        }

        public PacmanGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            boardSprite = new BoardSprite(gameControl.Board, this);
            charSprite = new CharacterSprite(this, gameControl);
            scoreBoard = new ScoreBoard(this, gameControl);
            Components.Add(boardSprite);
            Components.Add(charSprite);
            Components.Add(scoreBoard);

            graphics.PreferredBackBufferHeight = gameControl.Board.Height * 30 + 70;
            graphics.PreferredBackBufferWidth = gameControl.Board.Width * 30;
            graphics.ApplyChanges();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            gameTimer();

            // TODO: use this.Content to load your game content here
            base.LoadContent();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            if (gameOver)
            {
                // TODO: Add your update logic here
                if (counter == threshold)
                {
                    if (Keyboard.GetState().IsKeyDown(Keys.A))
                    {
                        gameControl.Pacman.move(MovementDirection.Left, gameControl.Board);
                    }
                    else if (Keyboard.GetState().IsKeyDown(Keys.D))
                    {
                        gameControl.Pacman.move(MovementDirection.Right, gameControl.Board);
                    }
                    else if (Keyboard.GetState().IsKeyDown(Keys.W))
                    {
                        gameControl.Pacman.move(MovementDirection.Up, gameControl.Board);
                    }
                    else if (Keyboard.GetState().IsKeyDown(Keys.S))
                    {
                        gameControl.Pacman.move(MovementDirection.Down, gameControl.Board);
                    }
                    counter = 0;
                }

                if (ghostCounter == ghostThreshold)
                {
                    if (gameControl.Energized && !energized)
                    {
                        energized = true;
                        timer.Stop();
                        energizerTimers = new Timer();
                        energizerTimers.Interval = 10000;
                        energizerTimers.Elapsed += energizedOver;
                        energizerTimers.Start();
                    }


                    if (energized) {
                        gameControl.frightenedGhost();
                    } else
                    {
                        if (scatter)
                        {
                            gameControl.scatterGhost();
                        }
                        else
                        {
                            gameControl.moveGhost();
                        }
                    }
                    
                    ghostCounter = 0;
                    base.Update(gameTime);
                }

                if (gameControl.checkGameOver())
                {
                    if(gameControl.Lives == 1)
                    {
                        gameOver = false;
                    }
                    else
                    {
                        gameControl.reset();
                        ghostPhases = 0;
                        timerHandler(null,null);
                        scatter = true;
                    }
                     
                }

                if (energized)
                    gameControl.checkIfGhostWasEaten();


                counter++;
                ghostCounter++;
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
