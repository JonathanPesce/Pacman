﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using PacmanLibrary;

namespace Pacman
{
    public class BoardSprite : DrawableGameComponent
    {
        private Board board;
        private SpriteBatch spriteBatch;
        private PacmanGame game;

        //Create all the image variables
        private Texture2D path;
        private Texture2D wall;
        private Texture2D point;
        private Texture2D energizer;

        public BoardSprite(Board board, PacmanGame pacman) : base(pacman) 
        {
            this.board = board;
            this.game = pacman;
        }

        /// <summary>
        /// I don't know what this does for now.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// This method will load all the images into the game.
        /// </summary>
        protected override void LoadContent()
        {
            this.spriteBatch = new SpriteBatch(GraphicsDevice);

            //Initialize all images
            this.path = game.Content.Load<Texture2D>("path");
            this.wall = game.Content.Load<Texture2D>("wall");
            this.point = game.Content.Load<Texture2D>("point");
            this.energizer = game.Content.Load<Texture2D>("Energizer");

            base.LoadContent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            this.spriteBatch.Begin();

            //Draw every element in the grid
            for (int x = 0; x < this.board.Width; x++)
            {
                for (int y = 0; y < this.board.Height; y++)
                {
                    var tile = board[x, y];
                    Texture2D image = null;

                    if (tile is Wall)
                    {
                        image = wall;
                    }
                    else if (tile is Path)
                    {
                        image = path;
                    }
                    else
                    {
                        image = path;
                    }

                    this.spriteBatch.Draw(image, new Rectangle(x * 30, (y * 30) + 35, 30, 30), Color.White);

                    if (tile is Path)
                        if (((Path)tile).pointAvailable)
                        {
                            if(((Path)tile).pointIsEnergizer)
                            {
                                this.spriteBatch.Draw(energizer, new Rectangle(x * 30, (y * 30) + 35, 30, 30), Color.White);
                            } else
                            {
                                this.spriteBatch.Draw(point, new Rectangle(x * 30, (y * 30) + 35, 30, 30), Color.White);
                            }
                        }
                }
            }

            this.spriteBatch.End();
            base.Draw(gameTime);
        }

    }
}